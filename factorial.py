#!/usr/bin/python3

def factorial(num):
    resultado = 1
    while num > 1:
        resultado = resultado * num
        num = num - 1
    return resultado

if __name__ == "__main__":
    for i in range(1, 11):
        print(factorial(i))