#!/usr/bin/python3

from sys import argv
from factorial import factorial

def suma(m, n):
    resultado = m + n
    return resultado

if __name__ == "__main__":
    if argv[1] == "factorial":
        fact = factorial(int(argv[2]))
        print(fact)
    elif argv[1] == "suma":
        try:
            sumanumero = suma(int(argv[2]), int(argv[3]))
            print(sumanumero)
        except ValueError:
            print("monstro, escribe bien los args")